<?php
/**
 * @package bug
 *
 */
require_once $_SERVER['DOCUMENT_ROOT'] . '/utilities/errorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/utilities/validateForm.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/utilities/dataSanitization.php';

require $_SERVER['DOCUMENT_ROOT'].'/model/bug.php';
use ErrorCodes\errorCode;
use Bug\Bug;
header('Access-Control-Allow-Origin: *');

/**
 * Function process the validation and send the data to model for adding the Bug data
 *
 * @return bool
 */
function addBug()
{
    if ('POST' == $_SERVER['REQUEST_METHOD'])
    {
        $FormData = array(
            'Severity' => $_POST['Severity'],
            'Title' => $_POST['Title'],
            'Description' => $_POST['Description'],
            'Reporter' => $_POST['Reporter'],
            'Programmer' => $_POST['Programmer']
        );
        $Required = array(
            'Severity',
            'Title',
            'Description',
            'Reporter',
            'Programmer'
        );
        if (validateFormData($FormData,$Required)) {

            applySanitization($FormData);
            $BugObj = new Bug();
            $ErrorObj = new errorCode();
            $Status = $BugObj->addBug(
                $FormData['Severity'],
                $FormData['Title'],
                $FormData['Description'],
                $FormData['Reporter'],
                $FormData['Programmer']
            );
            if (errorCode::EverythingOkay == $Status) {
                http_response_code(200);
                echo json_encode(array('Message' => 'Success adding bug'));
                return true;
            } else {
                http_response_code(422);
                echo json_encode(array('Error' => $ErrorObj->getErrorDetail($Status)));
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        http_response_code(400);
        echo json_encode(array('Error' =>  'Wrong Http method '));
        return false;
    }
}
addBug();







