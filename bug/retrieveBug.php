<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/2/19
 * Time: 5:16 PM
 *
 * @package bug
 */
header('Access-Control-Allow-Origin: *');
require $_SERVER['DOCUMENT_ROOT'] . '/utilities/errorCode.php';
require $_SERVER['DOCUMENT_ROOT'].'/model/bug.php';
use ErrorCodes\errorCode;
use Bug\Bug;

/**
 *  Function implements the retrieve bug functionality
 *  it check for request coming from client and request for the data to model class
 *
 * @return bool
 */
function retrieveBug()
{
    if ('GET' == $_SERVER['REQUEST_METHOD'])
    {
        $BugObj = new Bug();
        $ErrorObj = new errorCode();
        if ( empty($_GET['Id']) )
        {
            $Data = $BugObj->retrieveBug(
                array('Id','Time','Severity','Title','Description','ReporterId','ProgrammerId'),
                array(),
                array(),
                array()
            );
            if ( is_array($Data) )
            {
                http_response_code(200);
                echo json_encode(array('data'=>($Data)));
                return true;
            }
            else
            {
                http_response_code(400);
                echo json_encode(array('Error' =>  $ErrorObj->getErrorDetail($Data)));
                return false;
            }
        }
        else
        {
            if ( is_numeric($_GET['Id']))
            {
                $Id = $_GET['Id'];
            }
            else
            {
                http_response_code(422);
                echo json_encode(array('Error' => 'Invalid Id value'));
                return false;
            }
            $Data = $BugObj->retrieveBug(
                array('Time','Severity','Title','Description','ReporterId','ProgrammerId'),
                array('Id'),
                array('Id' => $Id),
                array()
            );
            if ( is_array($Data) )
            {
                http_response_code(200);
                echo json_encode(array('data'=>($Data)));
                return true;
            }
            else
            {
                http_response_code(400);
                echo json_encode(array('Error' =>  $ErrorObj->getErrorDetail($Data)));
                return false;
            }
        }
    }
    else
    {
        http_response_code(400);
        echo json_encode(array('Error' =>  'Wrong Http method '));
        return false;
    }
}
retrieveBug();