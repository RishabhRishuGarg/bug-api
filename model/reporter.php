<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/2/19
 * Time: 4:57 PM
 */

namespace Bug;


require_once $_SERVER['DOCUMENT_ROOT']."/model/getInterface.php";
require_once $_SERVER['DOCUMENT_ROOT']."/databaseOperations/databaseHandling.php";

use getInterface\getInterface;

use DatabaseHandling\DatabaseHandling;



class reporter implements getInterface
{
    private $DatabaseOperation;

    /**
     * programmer constructor.
     */
    function __construct()
    {
        $this->DatabaseOperation = new DatabaseHandling();
    }

    /**
     * @param $Name
     * @return int
     */
    public function getId($Name)
    {
        $Values = $this->DatabaseOperation->select(
            ReporterTableName,
            array("Id"),
            array("Name"),
            array("Name" => $Name),
            array()
        );
        return $Values;
    }
    public function getName($Id)
    {
        if ( "" == $Id )
        {
            $Values = $this->DatabaseOperation->select(
                ReporterTableName,
                array("Name"),
                array(),
                array(),
                array()
            );
        }
        else
        {
            $Values = $this->DatabaseOperation->select(
                ReporterTableName,
                array("Name"),
                array("Id"),
                array("Id" => $Id),
                array()
            );
        }
        return $Values;
    }
}