<?php
/**
 *
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/2/19
 * Time: 10:33 AM
 *
 * @package utilities
 */


/**
 * @param string , value of form data on which length has to check
 * @param integer , maximum length allowed for that value
 *
 * @return bool
 */
function checkLength($Name, $Length)
{
    return 3 < strlen($Name) && strlen($Name) <= $Length ? true : false;
}

/**
 *
 * @param $Severity, Severity value entered by user
 * @return bool
 *
 */
function checkSeverity($Severity)
{
    $SeverityOptions = array('Normal', 'High', 'Blocker', 'Enhancement', 'Critical');
    foreach ( $SeverityOptions as &$Option )
    {
        if ( $Option == $Severity)
        {
            return true;
        }
    }
    return false;
}

/**
 * Function validating the Bug form data and also checking for the length of data
 *
 * @return bool
 */
function validateFormData($FormData,$Required)
{



        foreach ($Required as &$Value) {
            if (empty($FormData[$Value])) {
                http_response_code(422);
                echo json_encode(array('Error' => $Value .  'Not found'));
                return false;
            }
        }

        if ( !checkSeverity($FormData['Severity']) )
        {
            http_response_code(422);
            echo json_encode(array('Error' => 'Invalid Severity option'));
            return false;
        }

        if ( !checkLength($FormData['Title'],100) )
        {
            http_response_code(422);
            echo json_encode(array('Error' => 'Invalid length','length'=> strlen($FormData['Title'])));
            return false;
        }

        if ( !checkLength($FormData['Description'],1000) )
        {
            http_response_code(422);
            echo json_encode(['Error' => 'Invalid Length','length'=> strlen($FormData['Description'])]);
            return false;
        }

    return true;
}