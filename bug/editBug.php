<?php
/**
 *
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/2/19
 * Time: 4:37 PM
 *
 * @package bug
 */
header('Access-Control-Allow-Origin: *');

require_once $_SERVER['DOCUMENT_ROOT'] . '/utilities/errorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/utilities/validateForm.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/utilities/dataSanitization.php';

require $_SERVER['DOCUMENT_ROOT'].'/model/bug.php';
use ErrorCodes\errorCode;
use Bug\Bug;


/**
 * Function implements the validation and submitting the send the data to model for editing.
 *
 * @return bool
 */
function editBug()
{
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
       /* parse_str(file_get_contents("php://input"), $post_vars);


        $FormData = json_decode(json_encode($post_vars),true);*/

        $FormData = array(
            'Severity' => $_POST['Severity'],
            'Title' => $_POST['Title'],
            'Description' => $_POST['Description'],
            'Reporter' => $_POST['Reporter'],
            'Programmer' => $_POST['Programmer'],
            'Id' => $_POST['Id']
        );
        $Required = array(
            'Severity',
            'Title',
            'Description',
            'Reporter',
            'Programmer',
            'Id'
        );

        if (validateFormData($FormData,$Required))
        {

            applySanitization($FormData);
            $BugObj = new Bug();
            $ErrorObj = new errorCode();
            $Status = $BugObj->editBug(
                $FormData['Severity'],
                $FormData['Title'],
                $FormData['Description'],
                $FormData['Reporter'],
                $FormData['Programmer'],
                $FormData['Id']
            );
            if (errorCode::EverythingOkay == $Status)
            {
                http_response_code(200);
                echo json_encode(array('Message' => 'Success Editing bug'));
                return true;
            }
            else
            {
                http_response_code(422);
                echo json_encode(array('Error' => $ErrorObj->getErrorDetail($Status)));
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        http_response_code(400);
        echo json_encode(array('Error' =>  'Wrong Http method '));
        return false;
    }
}
editBug();







