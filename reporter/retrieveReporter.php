<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/2/19
 * Time: 9:43 PM
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/utilities/errorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/model/reporter.php';

use ErrorCodes\errorCode;
use Bug\reporter;

header('Access-Control-Allow-Origin: *');
/**
 * @return bool
 */
function retrieveReporter()
{
    if ('GET' == $_SERVER['REQUEST_METHOD'])
    {
        $ReporterObj = new reporter();
        $ErrorObj = new errorCode();
        if ( empty($_GET['Id']) )
        {
            $Data = $ReporterObj->getName("");
            if ( is_array($Data) )
            {
                http_response_code(200);
                echo json_encode(array('data'=>($Data)));
                return true;
            }
            else
            {
                http_response_code(400);
                echo json_encode(array('Error' =>  $ErrorObj->getErrorDetail($Data)));
                return false;
            }
        }
        else
        {
            if ( is_numeric($_GET['Id']))
            {
                $Id = $_GET['Id'];
            }
            else
            {
                http_response_code(422);
                echo json_encode(array('Error' => 'Invalid Id value'));
                return false;
            }
            $Data = $ReporterObj->getName($Id);
            if ( is_array($Data) )
            {
                http_response_code(200);
                echo json_encode(array('data'=>($Data)));
                return true;
            }
            else
            {
                http_response_code(400);
                echo json_encode(array('Error' =>  $ErrorObj->getErrorDetail($Data)));
                return false;
            }
        }
    }
    else
    {
        http_response_code(400);
        echo json_encode(array('Error' =>  'Wrong Http method '));
        return false;
    }
}
retrieveReporter();