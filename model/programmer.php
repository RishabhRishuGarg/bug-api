<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/2/19
 * Time: 4:57 PM
 */
namespace Bug;

require_once $_SERVER['DOCUMENT_ROOT'] . "/model/getInterface.php";
require_once $_SERVER['DOCUMENT_ROOT']."/databaseOperations/databaseHandling.php";

use DatabaseHandling\DatabaseHandling;

use getInterface\getInterface;

class programmer implements getInterface
{

    private $DatabaseOperation;

    /**
     * programmer constructor.
     */
    function __construct()
    {
        $this->DatabaseOperation = new DatabaseHandling();
    }

    /**
     *  Function generates the request to database handler to get the Id of programmer from database
     *
     * @param $Name
     *
     * @return int or Associative array , Either the status or Id of programmer
     */
    public function getId($Name)
    {
        $Values = $this->DatabaseOperation->select(
            ProgrammerTableName,
            array("Id"),
            array("Name"),
            array("Name" => $Name),
            array()
        );
        return $Values;
    }

    /**
     * @param $Id
     * @return int
     */
    public function getName($Id)
    {

        if ( "" == $Id )
        {
            $Values = $this->DatabaseOperation->select(
                ProgrammerTableName,
                array("Name"),
                array(),
                array(),
                array()
            );
        }
        else
        {
            $Values = $this->DatabaseOperation->select(
                ProgrammerTableName,
                array("Name"),
                array("Id"),
                array("Id" => $Id),
                array()
            );

        }
        return $Values;
    }

}