<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/2/19
 * Time: 11:27 AM
 */

namespace DatabaseHandling;



use \ErrorCodes\errorCode;
use PDO;
require_once $_SERVER['DOCUMENT_ROOT']."/config/database.php";

/**
 *  handling all operation applied on database like insert update select
 *
 * @package databaseOperations
 *
 * */
class DatabaseHandling
{

    private $Conn;
    private $ErrorCodeObj;


    /**
     *  Create the connection to database set the Conn data memeber with database connection instance
     *
     * @return  integer Status of operation
     *
     * */
    public function getConnection()
    {
        if (NULL == $this->Conn)
        {   $this->Conn = new PDO("mysql:host=".ServerName.";dbname=".DatabaseName,
                DBUserName,
                DBPassword
            );
            // set the PDO error mode to exception
            if ( NULL == $this->Conn )
            {
                return errorCode::DatabaseConnectionError;
            }

            $this->Conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return errorCode::EverythingOkay;
        }
        else
        {
            return errorCode::EverythingOkay;
        }
    }
    /**
     * DatabaseHandling constructor.
     *
     * @return void
     *
     */
    function __construct()
    {
        $this->ErrorCodeObj = new errorCode();
        $StatusCode = $this->getConnection();
        $this->ErrorCodeObj->getErrorDetail($StatusCode);

    }

    /**
     *  Function implements the insertion operation in database
     *
     * @param $TableName ,String table name on which insertion operation has to made
     * @param $Attributes array consist the attribute name to be store in database
     * @param $Values ,array consist the values of all attributes to be store in database
     *
     * @return Integer which defines the status of operation
     *
     */
    public function insert($TableName, $Attributes, $Values)
    {
        if ( ! count($Attributes) == count($Values) )
        {
            return errorCode::WrongArgumentPassed;
        }
        $Query = "INSERT INTO ".$TableName."(";
        for ( $i=0; $i<count($Attributes); $i++ )
        {
            if ( !0 == $i)
            {
                $Query = $Query.",";
            }
            $Query = $Query. $Attributes[$i];
        }
        $Query = $Query.") VALUES(";
        for ( $i=0; $i<count($Attributes); $i++ )
        {
            if ( !0 == $i)
            {
                $Query = $Query.",";
            }
            $Query = $Query."?";
        }
        $Query = $Query.");";
        $Stmt = $this->Conn->prepare($Query);
        if ( $Stmt->execute($Values) )
        {
            return errorCode::EverythingOkay;
        }
        else
        {
            return errorCode::DatabaseInsertionError;
        }

    }


    /**
     *  Function implements the Modify operation on database
     *
     * @param $TableName , Table name on which operation has to made
     * @param $ColumnAttributes, attributes on which changes has to made
     * @param $ConditionAttributes, attributes on which condition has to check
     * @param $Values , values of all attributes as associative array
     * @param $Conditions , Condition operators which combines the multiple conditions
     *
     * @return Integer Status of operation
     */
    public function modify(
        $TableName,
        $ColumnAttributes,
        $ConditionAttributes,
        $Values,
        $Conditions
    )
    {
        $Query = "UPDATE ".$TableName." SET ";
        for ( $i=0; $i<count($ColumnAttributes); $i++ )
        {
            if ( !0 == $i )
            {
                $Query = $Query." , ";
            }
            $Query = $Query.$ColumnAttributes[$i]."=:".$ColumnAttributes[$i];
        }
        $Query = $Query." WHERE ";

        for ( $i=0; $i < count($ConditionAttributes); $i++)
        {
            if ( !0 == $i )
            {
                $Query = $Query." ".$Conditions[$i]." ";
            }
            $Query = $Query.$ConditionAttributes[$i]."=:".$ConditionAttributes[$i];
        }
        $Query = $Query.";";

        $Stmt = $this->Conn->prepare($Query);
        if ( $Stmt->execute($Values) )
        {
            return errorCode::EverythingOkay;
        }
        else
        {
            return errorCode::DatabaseInsertionError;
        }
    }

    /**
     *  Function Implements the retrieve(Select) operation from database
     *
     * @param $TableName
     * @param $ColumnAttributes
     * @param $ConditionAttributes
     * @param $Values
     * @param $Conditions
     *
     * @return int, array
     */
    public function select(
        $TableName,
        $ColumnAttributes,
        $ConditionAttributes,
        $Values,
        $Conditions
    )
    {
        $Query = "SELECT ";
        for ( $i=0; $i<count($ColumnAttributes); $i++ )
        {
            if ( !0 == $i )
            {
                $Query = $Query." , ";
            }
            $Query = $Query.$ColumnAttributes[$i];
        }
        $Query = $Query." FROM ".$TableName;
        if ( 0 < count($ConditionAttributes))
        {
            $Query = $Query." WHERE ";
        }

        for ( $i=0; $i < count($ConditionAttributes); $i++)
        {
            if ( !0 == $i )
            {
                $Query = $Query." ".$Conditions[$i]." ";
            }
            $Query = $Query.$ConditionAttributes[$i]."=:".$ConditionAttributes[$i];
        }
        $Query = $Query.";";

        $Stmt = $this->Conn->prepare($Query);
        if ( $Stmt->execute($Values) )
        {
            $Stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $Stmt->fetchAll();
        }
        else
        {
            return errorCode::DatabaseInsertionError;
        }

    }
}
