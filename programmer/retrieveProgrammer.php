<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/2/19
 * Time: 9:43 PM
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/utilities/errorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/model/programmer.php';
require_once $_SERVER['DOCUMENT_ROOT']."/databaseOperations/databaseHandling.php";

use ErrorCodes\errorCode;
use Bug\programmer;

header('Access-Control-Allow-Origin: *');
function retrieveProgrammer()
{
    if ('GET' == $_SERVER['REQUEST_METHOD'])
    {
        $ProgrammerObj = new programmer();
        $ErrorObj = new errorCode();
        if ( empty($_GET['Id']) )
        {
            $Data = $ProgrammerObj->getName("");

            if ( is_array($Data) )
            {
                http_response_code(200);
                echo json_encode(array('data'=>($Data)));
                return true;
            }
            else
            {
                http_response_code(400);
                echo json_encode(array('Error' =>  $ErrorObj->getErrorDetail($Data)));
                return false;
            }
        }
        else
        {
            if ( is_numeric($_GET['Id']))
            {
                $Id = $_GET['Id'];
            }
            else
            {
                http_response_code(422);
                echo json_encode(array('Error' => 'Invalid Id value'));
                return false;
            }
            $Data = $ProgrammerObj->getName($Id);
            if ( is_array($Data) )
            {
                http_response_code(200);
                echo json_encode(array('data'=>($Data)));
                return true;
            }
            else
            {
                http_response_code(400);
                echo json_encode(array('Error' =>  $ErrorObj->getErrorDetail($Data)));
                return false;
            }
        }
    }
    else
    {
        http_response_code(400);
        echo json_encode(array('Error' =>  'Wrong Http method '));
        return false;
    }
}
retrieveProgrammer();