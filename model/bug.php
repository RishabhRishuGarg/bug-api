<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/2/19
 * Time: 11:01 AM
 */

namespace Bug;
require_once $_SERVER['DOCUMENT_ROOT']."/databaseOperations/databaseHandling.php";

require_once $_SERVER['DOCUMENT_ROOT'] . "/model/programmer.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/model/reporter.php";


use DatabaseHandling\DatabaseHandling;
use ErrorCodes\errorCode;


class Bug
{

    private $DatabaseOperation ;
    private $ProgrammerObj;
    private $ReporterObj;

    /**
     * Bug constructor.
     */
    function __construct()
    {
        $this->DatabaseOperation = new DatabaseHandling();
        $this->ProgrammerObj = new programmer();
        $this->ReporterObj = new reporter();
    }

    /**
     *  Function generate the request to database handler to edit the data
     *
     * @param $Severity
     * @param $Title
     * @param $Description
     * @param $ReporterName
     * @param $ProgrammerName
     * @param $Id
     *
     * @return Integer Status of operations
     */
    public function editBug(
        $Severity,
        $Title,
        $Description,
        $ReporterName,
        $ProgrammerName,
        $Id
    )
    {
        $ReporterId ;
        $ProgrammerId ;

        $ReporterData   = $this->ReporterObj->getId($ReporterName);
        if (is_array($ReporterData) )
        {
            if(empty($ReporterData))
            {
                return errorCode::WrongArgumentPassed;
            }
            else
            {
                $ReporterId = $ReporterData[0]["Id"];
            }
        }
        else
        {
            return $ReporterData;
        }

        $ProgrammerData = $this->ProgrammerObj->getId($ProgrammerName);
        if (is_array($ProgrammerData))
        {
            if(empty($ProgrammerData))
            {
                return errorCode::WrongArgumentPassed;
            }
            else
            {
                $ProgrammerId = $ProgrammerData[0]["Id"];
            }
        }
        else
        {
            return $ProgrammerData;
        }

        $Status =$this->DatabaseOperation->modify(
            BugTableName,
            array("Severity", "Title", "Description", "ReporterId","ProgrammerId"),
            array("Id"),
            array(
                "Severity"     => $Severity,
                "Title"        => $Title,
                "Description"  => $Description,
                "ReporterId"   => $ReporterId,
                "ProgrammerId" => $ProgrammerId,
                "Id"           => $Id
            ),
            array()
        );
        return $Status;
    }

    /**
     *  Function implements the add of new blog in database .Generates the request to database handler to
     *  add new data in Bugs table
     *
     * @param $Severity
     * @param $Title
     * @param $Description
     * @param $ReporterName
     * @param $ProgrammerName
     * @param $Id
     *
     * @return integer Status of operation
     */
    public function addBug(
        $Severity,
        $Title,
        $Description,
        $ReporterName,
        $ProgrammerName
    )
    {
        $ReporterId ;
        $ProgrammerId ;

        $ReporterData   = $this->ReporterObj->getId($ReporterName);
        if (is_array($ReporterData) )
        {
            if(empty($ReporterData))
            {
                return errorCode::WrongArgumentPassed;
            }
            else
            {
                $ReporterId = $ReporterData[0]["Id"];
            }
        }
        else
        {
            return $ReporterData;
        }

        $ProgrammerData = $this->ProgrammerObj->getId($ProgrammerName);
        if (is_array($ProgrammerData))
        {
            if(empty($ProgrammerData))
            {
                return errorCode::WrongArgumentPassed;
            }
            else
            {
                $ProgrammerId = $ProgrammerData[0]["Id"];
            }
        }
        else
        {
            return $ProgrammerData;
        }
        $Status = $this->DatabaseOperation->insert(
            BugTableName,
            array("Severity", "Title", "Description", "ReporterId", "ProgrammerId"),
            array($Severity, $Title, $Description, $ReporterId, $ProgrammerId)
            );
        return $Status;
    }


    /**
     *  Function handle the request of retrieving the Bug data from database. Generates the request to
     *  retrieve the data
     *
     * @param $Attributes, Array which data to gather from database
     * @param $ConditionAttributes,  Array of attributes on which condition has be applied
     * @param $Values, Associative array values of all attributes
     * @param $Conditions, Conditional operator to merge two condition
     *
     * @return int or array , Status value or the data as an Associative array
     */
    public function retrieveBug($Attributes, $ConditionAttributes, $Values, $Conditions)
    {
        $BugData = $this->DatabaseOperation->select(
            BugTableName,
            $Attributes,
            $ConditionAttributes,
            $Values,
            $Conditions
        );
        return $BugData;
    }
}
