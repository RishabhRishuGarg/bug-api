<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 19/2/19
 * Time: 12:10 PM
 */

namespace ErrorCodes;

/**
 *  Responsible for handling the error code and getting their descriptions
 * @package utilities
 * */
class errorCode
{
    public const DatabaseConnectionError = 1001;
    public const DatabaseInsertionError  = 1002;
    public const WrongArgumentPassed     = 1101;
    public const EverythingOkay          = 2000;


    /**
     *  Map the error code with its description
     *
     * @param integer contains the error code
     *
     * @return string containing the description of Error code
     * */
    public function getErrorDetail($Error)
    {
        switch ($Error)
        {
            case self::DatabaseConnectionError :
                return "Database connection error occured";

            case self::EverythingOkay:
                return "Everything worked fine";

            case self::WrongArgumentPassed:
                return "Invalid information passed in function argument";

            case self::DatabaseInsertionError:
                return "Database value insertion error occured";

            default :
                return "Invalid code";
        }
    }

}