<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 20/2/19
 * Time: 2:50 PM
 */

/**
 * Function convert the html character into some ASCII values
 *
 * @var array $FormData
 *
 *
 */
function applySanitization(&$FormData)
{
    $FormData['Title'] =  htmlentities($FormData['Title'], ENT_QUOTES);
    $FormData['Description'] = htmlentities($FormData['Description'], ENT_QUOTES);
}